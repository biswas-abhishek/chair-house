//Include Express.js
import express from "express";
import path from "path";
import dotenv from "dotenv";

const app = express();
dotenv.config();

// Sending HTML File
app.get("/", (req, res) =>
  res.sendFile("./src/index.htm", { root: __dirname })
);

// Sending CSS And Image

app.use(express.static(path.join(__dirname, "/src")));

// Adding Normailze CSS
/*
! File Add When Work In Development Otherwise It Compile In Production Into Dist Folder
*/
if (process.env.ENVIRONMENT === "Development") {
  app.use(
    "/normalize",
    express.static(path.join(__dirname, "node_modules/normalize.css"))
  );
}

// Listening
app.listen(process.env.PORT, process.env.APP_URL, () =>
  console.log(`listening on port ${process.env.PORT}!
  Local : http://${process.env.APP_URL}:${process.env.PORT}
  `)
);
