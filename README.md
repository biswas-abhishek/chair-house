# Chair House
## Buy Chair In Affordable Price

[![Netlify Status](https://api.netlify.com/api/v1/badges/60a4da35-f6cf-4e42-b337-3eb077503ea7/deploy-status)](https://app.netlify.com/sites/chair-house/deploys)

# Web Technologies I Used
![HTML](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/39da008e57dcebf7f5c3dcdad7b7b8674956385d/img/html.svg)![CSS](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/39da008e57dcebf7f5c3dcdad7b7b8674956385d/img/css.svg)
# Other Tools
![Gitlab](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/39da008e57dcebf7f5c3dcdad7b7b8674956385d/img/Gitlab.svg)![Netlify](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/39da008e57dcebf7f5c3dcdad7b7b8674956385d/img/netlify.svg)![Gulp](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/39da008e57dcebf7f5c3dcdad7b7b8674956385d/img/gulp.svg)

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (Version 12 or greater recommended)
- [Git](https://git-scm.com/)
- [Gulp](https://gulpjs.com/)
---
run :
##### If using HTTP
```sh
git clone https://gitlab.com/biswas-abhishek/chair-house.git
```

##### If using SSH
```sh
git clone git@gitlab.com:biswas-abhishek/chair-house.git
```
```bash
cd chair-house
```
```bash
cp env .env
```
#### Add Port:8000, Host:localhost and ENVIRONMENT=Development

```bash
npm start
```
Open Browser and `Enter` URL :
```
http://localhost:8000
```

To create compressed, production-ready assets, run `npm run build`.

## URL

### Gitlab - https://gitlab.com/biswas-abhishek/chair-house 
### WebSite - https://chair-house.netlify.app/
