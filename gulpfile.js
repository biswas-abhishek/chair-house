import gulp from "gulp"; //Gulp
// HTML Minify Plugin
import htmlmin from "gulp-htmlmin";
// CSS Autoprefixer Plugin
import autoprefixer from "gulp-autoprefixer";
// Image Minify Plugin
import imagemin from "gulp-imagemin";
import imageminMozjpeg from "imagemin-mozjpeg";
import imageminsvgo from "imagemin-svgo";
import imageminOptipng from "imagemin-optipng";

// CSS Minify Plugin
import cleanCSS from "gulp-clean-css";
//CSS Lint Plugin
import gulpStylelint from "gulp-stylelint";
//Concat CSS File
import concat from "gulp-concat";

//HTML Minify
gulp.task("html-minify", () => {
  return gulp
    .src("src/*.htm")
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("dist"));
});

//Image Minify
gulp.task("jpg-min", () => {
  return gulp
    .src("src/**/*.jpg")
    .pipe(
      imagemin(imageminMozjpeg({ quality: 80, progressive: true })).pipe(
        gulp.dest("dist")
      )
    );
});
gulp.task("svg-min", () => {
  return gulp.src("src/**/*.svg").pipe(
    imagemin(
      imageminsvgo({
        plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
      })
    ).pipe(gulp.dest("dist/"))
  );
});
gulp.task("png-min", () => {
  return gulp
    .src("src/**/*.png")
    .pipe(
      imagemin(imageminOptipng({ optimizationLevel: 5 })).pipe(
        gulp.dest("dist")
      )
    );
});

//CSS Minify
gulp.task("css-minify", () => {
  return gulp
    .src(["node_modules/normalize.css/normalize.css", "src/**/*.css"])
    .pipe(concat("assets/style/style.css"))
    .pipe(cleanCSS({ compatibility: "ie7", level: 2 }))
    .pipe(
      autoprefixer({
        cascade: false,
      }),
      "last 4 version"
    )
    .pipe(gulp.dest("dist"));
});

//Copy JSON File
gulp.task("json-cp", () => {
  return gulp.src("src/**/site.webmanifest").pipe(gulp.dest("dist"));
});

//CSS Lint
gulp.task("css-lint", () => {
  return gulp
    .src("src/**/**/style.css")
    .pipe(
      gulpStylelint({
        reporters: [{ formatter: "string", console: true }],
      })
    )
    .on("finish", () => {
      console.log("Test Complete");
      console.log("No Error Found");
    });
});

//Test
gulp.task("test", gulp.series("css-lint"));

//
gulp.task(
  "build",
  gulp.series(
    "html-minify",
    "jpg-min",
    "svg-min",
    "css-minify",
    "png-min",
    "json-cp"
  )
);
